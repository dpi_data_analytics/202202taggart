# activate the env

# allow the shell to access conda
CONDA_BASE=$(conda info --base)
source $CONDA_BASE/etc/profile.d/conda.sh

# create output dirs
mkdir -p analyses/plots

conda activate amplicon-r

Rscript toxo-gut-microbiome-study-v3.0.R 2>&1 | tee analyses.log

conda deactivate
