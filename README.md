# 202202taggart  

## Introduction   
Does infection with toxoplasma change the rat microbiome?  

## The study design  
See the metadata directory for the sample sheet.  

| Sample         | Sex              | Treatment        |
| -------------- | ---------------- | ---------------- |
| NC1.16S.V3.V4  | Negative_control | Negative_control |
| NC2.16S.V3.V4  | Negative_control | Negative_control |
| 2AF.16S.V3.V4  | F                | Placebo          |
| 2AM.16S.V3.V4  | M                | Placebo          |
| 2BF.16S.V3.V4  | F                | Placebo          |
| 2BM.16S.V3.V4  | M                | Placebo          |
| 2CM.16S.V3.V4  | M                | Placebo          |
| 3AF.16S.V3.V4  | F                | Placebo          |
| 3BF.16S.V3.V4  | F                | Placebo          |
| 4AM.16S.V3.V4  | M                | Placebo          |
| 4BM.16S.V3.V4  | M                | Placebo          |
| 4CM.16S.V3.V4  | M                | Placebo          |
| 6AF.16S.V3.V4  | F                | Placebo          |
| 6AM.16S.V3.V4  | M                | Placebo          |
| 6BF.16S.V3.V4  | F                | Placebo          |
| 6BM.16S.V3.V4  | M                | Placebo          |
| 6CM.16S.V3.V4  | M                | Placebo          |
| 8AM.16S.V3.V4  | M                | Placebo          |
| 8BM.16S.V3.V4  | M                | Placebo          |
| 8CM.16S.V3.V4  | M                | Placebo          |
| 10AF.16S.V3.V4 | F                | Placebo          |
| 10BF.16S.V3.V4 | F                | Placebo          |
| 11AF.16S.V3.V4 | F                | Placebo          |
| 11BF.16S.V3.V4 | F                | Placebo          |
| 12AF.16S.V3.V4 | F                | Placebo          |
| 12BF.16S.V3.V4 | F                | Placebo          |
| 1AF.16S.V3.V4  | F                | Toxo             |
| 1AM.16S.V3.V4  | M                | Toxo             |
| 1BF.16S.V3.V4  | F                | Toxo             |
| 1BM.16S.V3.V4  | M                | Toxo             |
| 1CM.16S.V3.V4  | M                | Toxo             |
| 3AM.16S.V3.V4  | M                | Toxo             |
| 3BM.16S.V3.V4  | M                | Toxo             |
| 3CM.16S.V3.V4  | M                | Toxo             |
| 4AF.16S.V3.V4  | F                | Toxo             |
| 4BF.16S.V3.V4  | F                | Toxo             |
| 5AF.16S.V3.V4  | F                | Toxo             |
| 5AM.16S.V3.V4  | M                | Toxo             |
| 5BF.16S.V3.V4  | F                | Toxo             |
| 5BM.16S.V3.V4  | M                | Toxo             |
| 5CM.16S.V3.V4  | M                | Toxo             |
| 7AF.16S.V3.V4  | F                | Toxo             |
| 7AM.16S.V3.V4  | M                | Toxo             |
| 7BF.16S.V3.V4  | F                | Toxo             |
| 7BM.16S.V3.V4  | M                | Toxo             |
| 7CM.16S.V3.V4  | M                | Toxo             |
| 8AF.16S.V3.V4  | F                | Toxo             |
| 8BF.16S.V3.V4  | F                | Toxo             |
| 9AF.16S.V3.V4  | F                | Toxo             |
| 9BF.16S.V3.V4  | F                | Toxo             |


## Generating Amplicon Sequence Variant counts using qiime2  

### Conda qiime2 env  
A conda env for this project is found in `env/qiime2-2021.11-py38-linux-conda.yml`, this can be cloned with `conda env create -f env/qiime2-2021.11-py38-linux-conda.yml`.  

Activate the conda env using: `conda activate qiime2-2021.11`.  

### Import the data and generate a data summary  
Imported the data into QIIME using the manifest file `qiime2/manifest.tsv.txt`.  

```
#head qiime2/manifest.tsv.txt
##sample-id       forward-absolute-filepath       reverse-absolute-filepath
##10AF
##/home/dwheeler/ngbs/projects/202202taggart/202202taggart/data/all_data/10AF-16S_V3-V4_CTDC5_AAGAGGCA-GCGTAAGA_L001_R1.fastq
##/home/dwheeler/ngbs/projects/202202taggart/202202taggart/data/all_data/10AF-16S_V3-V4_CTDC5_AAGAGGCA-GCGTAAGA_L001_R2.fastq
##10BF
##/home/dwheeler/ngbs/projects/202202taggart/202202taggart/data/all_data/10BF-16S_V3-V4_CTDC5_GTAGAGGA-GCGTAAGA_L001_R1.fastq
##/home/dwheeler/ngbs/projects/202202taggart/202202taggart/data/all_data/10BF-16S_V3-V4_CTDC5_GTAGAGGA-GCGTAAGA_L001_R2.fastq

```
The `scripts/import_reads.sh` script reads the data and writes a `qza` qiime data object file. The second part of the script generates plots related to the raw data.   

### Check that the primers can be found in the amplicon  
The primers used in this study were 341F and 806R.  

```
#cat fastq_info/primers/primers_merged.txt
##>341F 16S 17nt
##CCTAYGGGRBGCASCAG
##>806R 16S 20nt
##GGACTACNNGGGTATCTAAT
```


```
#head -n2 ../data/all_data/1CM-16S_V3-V4_CTDC5_GGACTCCT-TCGACTAG_L001_R1.fastq
##@M00566:295:000000000-CTDC5:1:1102:9891:1770 1:N:0:GGACTCCT+TCGTCTAG
##<--CCTACGGGAGGCAGCAG-->TGGGGAATATTGCACAATGTTGGAAACCCTGATGCAGCGACGCCGCGTGAGTGAAGAAGTATTTCTGTTTGTAAAGCTCTATCAGCAGGGAAGAAAATTTCTTTTCCTGTCTAAGAAGCCCCTGCTAACTACGTGCCAGCAGCCGCGGTAATTCGTAGGGGGCCAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCTTTTTCTTCTTTTCTAGTCTGTTTTGAAAGCCCGCTGCTCAACTCCTGGACTTCTTTTTTTTCTTCCTTGCTGGTGTTCAGGAGA
#head -n2 ../data/all_data/1CM-16S_V3-V4_CTDC5_GGACTCCT-TCGACTAG_L001_R2.fastq
##@M00566:295:000000000-CTDC5:1:1102:9891:1770 2:N:0:GGACTCCT+TCGTCTAG
##<--GGACTACAGGGGTATCTAAT-->CCTGTTTGCTCCCCACTCTTTCTTTCCTCAACGTCAGTTACAGTCCAGTACTCCTCCTTCTCCACTGGTGTTCCTCCTGATATCTACGCATTTCACCGCTACACCTTTAATTCCTCTTACCTCTCCTGCACTCCATCCTTCCCGTTTCCAATGCAGTCCCGCATTTTCGCCCCCGTCTTTCCCTTCTTTCTTGCTCTTCCGTCTACCCTCCCTTTCCTCCCCTTCCTTCCCGTTCACGCTTTCCCCCTACGTCTTCCCGCGGCTTCTTGCCCCTTTTTCT
```

Use `qiime cutadapt trim-paried` command to remove the primers. The parameters are shown in `script/qiime-trim.sh`.   

Check the output by exporting the reads to a tmp directory and viewing the
results before and after trimming for a R1 and R2.  

```
#qiime tools export --input-path paired-end-demux-trimmed.qza --output-path ./tmp

## Has removed the forward primer from the R1!
# head -n2 ../../data/all_data/10AF-16S_V3-V4_CTDC5_AAGAGGCA-GCGTAAGA_L001_R1.fastq
##@M00566:295:000000000-CTDC5:1:1102:15344:1841 1:N:0:AAGAGGCA+TCGTAAGA
##CCTACGGGGGGCAGCAGTGGGGAATATTGCACAATGGGGGAAACCCTGATGCAGCGACGCCGCGTGAGTGAAGAAGTTTTTCTGTTTGTAAAGCTCTATCAGCAGGGAAGATAATGTCGTTTCCTGACTAAGAAGCCCCGGCTAACTACGTGCCAGCAGCCGCGGTTATTCTTAGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCTTTTTTTGCTTTTCTTGTCAGATTTGAAAACCCGTGGCTCAACTCCGGGATTTCTTTTTTTTCTTTCTTGCTGTTGTGCAGGAGG
#zcat ./tmp/10AF_0_L001_R1_001.fastq.gz | head -n 2 
##@M00566:295:000000000-CTDC5:1:1102:15344:1841 1:N:0:AAGAGGCA+TCGTAAGA
##TGGGGAATATTGCACAATGGGGGAAACCCTGATGCAGCGACGCCGCGTGAGTGAAGAAGTTTTTCTGTTTGTAAAGCTCTATCAGCAGGGAAGATAATGTCGTTTCCTGACTAAGAAGCCCCGGCTAACTACGTGCCAGCAGCCGCGGTTATTCTTAGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCTTTTTTTGCTTTTCTTGTCAGATTTGAAAACCCGTGGCTCAACTCCGGGATTTCTTTTTTTTCTTTCTTGCTGTTGTGCAGGAGG
```

And the R2.  

```
# The reverse primer has been removed
#head -n2 ../../data/all_data/10AF-16S_V3-V4_CTDC5_AAGAGGCA-GCGTAAGA_L001_R2.fastq
##@M00566:295:000000000-CTDC5:1:1102:15344:1841 2:N:0:AAGAGGCA+TCGTAAGA
##GGACTACGTGGGTATCTAATCCTGTTTGCTCCCCACGCTTTCTTGCCTCAGTGTCAGTTACAGTCCAGTAATCCTCCTTCGCCACTGGTGTTCCTCCTAATATCTATGCATTTCACCGCTACCCTACTAATTCCTCTTACCTCTCCTGCACTCCAGCCCCCCCGTTTCCAATGCAATCCCGGAGTTCCGCCCCTGTTTTTCCCTTCTTCCTTTCCCTGCCACCTCCGCTCCCTTTCCCCCCCTTCCTTCCTCTTAACGCTTTCCCCCTACGTTTTCCCTCGCTTCCTGGCCCCTATTTCT
#zcat ./tmp/10AF_50_L001_R2_001.fastq.gz | head -n2
##@M00566:295:000000000-CTDC5:1:1102:15344:1841 2:N:0:AAGAGGCA+TCGTAAGA
##CCTGTTTGCTCCCCACGCTTTCTTGCCTCAGTGTCAGTTACAGTCCAGTAATCCTCCTTCGCCACTGGTGTTCCTCCTAATATCTATGCATTTCACCGCTACCCTACTAATTCCTCTTACCTCTCCTGCACTCCAGCCCCCCCGTTTCCAATGCAATCCCGGAGTTCCGCCCCTGTTTTTCCCTTCTTCCTTTCCCTGCCACCTCCGCTCCCTTTCCCCCCCTTCCTTCCTCTTAACGCTTTCCCCCTACGTTTTCCCTCGCTTCCTGGCCCCTATTTCT
```


### Denoising with DADA2  
See https://github.com/allenlab/QIIME2_16S_ASV_protocol.  

This step filter out noisy sequences, correct errors in marginal sequences (in the case of DADA2), remove chimeric sequences, remove singletons, join denoised paired-end reads (in the case of DADA2), and then dereplicate those sequences.   

Based on the primer locations the amplicon length will be ~440bp, there should be a good overlap with 300 bp reads, but I really want to trim off as much of the lower quality data that I can. I'll try a few different settings for the trim paramaters to see which gives a good balance for keeping data (ie finding overlaps given a 20 bp min overlap is req to join pairs) and removing low quality data. This step also filters out PCR artificats, which will also result in a loss of data.   

The final DADA2 options were (for full run params see `scripts/qiime-dada2.sh`):    
```
--p-trunc-len-f 240  
--p-trunc-len-r 220  
--p-max-ee-f 2  
--p-max-ee-r 4  
```

### Training a classifier for the taxonmomy  
The silva compatible files can be found https://docs.qiime2.org/2020.8/data-resources/?highlight=silva.  

Download the full length sequences (https://data.qiime2.org/2020.8/common/silva-138-99-seqs.qza) and taxonomy (https://data.qiime2.org/2020.8/common/silva-138-99-tax.qza) so that I can extract the target amplicon.   

Fit the model and then train it on the amplicon used in this study. Note the train step uses a lot of RAM (>100GB).  

The two scripts are `/scripts/qiime-extract-amplicon.sh` (you need the primer sequences for this step) and `scripts/qiime-train-classifier.sh`.  

## PhytoREF (NOTE THIS WAS NOT DONE HERE FOR REFERENCE ONLY)  
I didn't do this, but it looks handy in future work.   

Taken from
https://github.com/allenlab/QIIME2_16S_ASV_protocol/blob/master/README.md.

```
qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path ./db/phytoref/PhytoRef_with_taxonomy_simple.fasta  \
  --output-path ./db/phytoref/phytoref.qza

qiime tools import \
  --type 'FeatureData[Taxonomy]' \
  --input-format HeaderlessTSVTaxonomyFormat \
  --input-path ./db/phytoref/PhytoRef_taxonomy.tsv \
  --output-path ./db/phytoref/phytoref_tax.qza

qiime feature-classifier extract-reads \
  --i-sequences ./db/phytoref/phytoref.qza \
  --p-f-primer GTGYCAGCMGCCGCGGTAA \
  --p-r-primer CCGYCAATTYMTTTRAGTTT \
  --o-reads ./db/phytoref/phytoref_extracts.qza
  
qiime feature-classifier fit-classifier-naive-bayes \
  --i-reference-reads ./db/phytoref/phytoref_extracts.qza \
  --i-reference-taxonomy ./db/phytoref/phytoref_tax.qza \
  --o-classifier ./db/phytoref/phytoref_classifier.qza
Classify ASVs:

qiime feature-classifier classify-sklearn \
  --p-n-jobs -1 \
  --i-classifier ./db/phytoref/phytoref_classifier.qza \
  --i-reads merged_rep-seqs.qza \
  --o-classification phytoref_tax_sklearn.qza

qiime tools export \
  --input-path phytoref_tax_sklearn.qza \
  --output-path asv_tax_dir

mv asv_tax_dir/taxonomy.tsv asv_tax_dir/phytoref_taxonomy.tsv

```

## The final output tables is via R  
Run `scripts/create_outputs.R`.  See the script for examples of the outputs.  

### R dependency hints....   
See (https://library.qiime2.org/plugins/q2-breakaway/9/) for using `conda bioconductor-xxxx` to install R packages to avoid dependency hell.  

The conda environment for the amplicon analyses.  

```
conda env create -f env/amplicon-r.yml
```

### Update the analyses with the new ASV table and taxonomy from QIIME2.  

Use QIIME2 ASV's in place of the original USEARCH OTUs, these use the updated SILVA database taxonomy in place of greengenes see `R/toxo-gut-microbiome-study-v3.0.R`; outputs from this script will be written to `R/analyses`.        

This script will log the stdout to a log file.  
```
./run_analyses.sh
```

## ANCOM-BC  
Update the original DESeq2 differential abundance using ANCOM-BC. This was integrated into the `R/toxo-gut-microbiome-study-v3.0.R` script.
