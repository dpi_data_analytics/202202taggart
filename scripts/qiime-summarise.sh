#!/bin/bash  

# summarise and export  

qiime tools export \
  --input-path table-dada2.qza \
  --output-path asv_table

biom convert -i ./asv_table/feature-table.biom \
  -o ./asv_table/asv-table.tsv \
  --to-tsv

qiime tools export \
  --input-path rep-seqs-dada2.qza \
  --output-path asvs

qiime feature-table tabulate-seqs \
  --i-data rep-seqs-dada2.qza \
  --o-visualization rep-seqs-dada2.qzv

