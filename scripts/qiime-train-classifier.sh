#!/bin/bash
# https://docs.qiime2.org/2019.10/tutorials/feature-classifier/
# https://github.com/allenlab/QIIME2_16S_ASV_protocol/blob/master/README.md 
# train the classifier using our region

qiime feature-classifier classify-sklearn \
  --p-n-jobs 12 \
  --i-classifier ./db/silva/silva-138-99-classifier.qza \
  --i-reads rep-seqs-dada2.qza \
  --o-classification silva_tax_sklearn.qza

echo "exporting..."

qiime tools export \
  --input-path silva_tax_sklearn.qza \
  --output-path asv_tax_dir

mv asv_tax_dir/taxonomy.tsv asv_tax_dir/silva_taxonomy.tsv
