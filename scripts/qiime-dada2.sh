#!/bin/bash  

# data2 denoise

mkdir asvs

qiime dada2 denoise-paired \
    --i-demultiplexed-seqs paired-end-demux-trimmed.qza \
    --p-n-threads 0 \
    --p-trunc-len-f 240 \
    --p-trunc-len-r 220 \
    --p-max-ee-f 2 \
    --p-max-ee-r 4 \
    --p-n-reads-learn 1000000 \
    --p-chimera-method consensus \
    --o-table ./asvs/table-dada2.qza \
    --o-representative-sequences ./asvs/rep-seqs-dada2.qza \
    --o-denoising-stats ./asvs/stats-dada2.qza \
    --verbose >> dada2.log 2>&1

qiime metadata tabulate \
    --m-input-file ./asvs/stats-dada2.qza \
    --o-visualization ./asvs/stats-dada2.qzv
