#!/usr/bin/env python
import sys
import csv

# add a taxonomy column onto the dataframe based on a key
try:
    table = sys.argv[1]
    # tab delin: hash key\ttaxonomy
    taxa = sys.argv[2]
except:
    print("usage:python add_taxonomy.py <table> <taxa-key>")
    sys.exit(1)

# taxa table
taxa_dict = {}
with open(taxa) as rf:
    header = next(rf)
    for line in rf:
        # hash key for species tag
        bits = line.strip().split('\t')
        key = bits[0]
        taxonomy = bits[1]
        taxa_dict[key] = taxonomy

# now parse the table and add taxonomy based on taxa_id col

outfile = table.split('.')[0] + '_taxonomy.csv'
outfile_h = open(outfile, 'w')
csv_writer = csv.writer(outfile_h)

with open(table) as rf:
    csv_reader = csv.reader(rf)
    header = next(csv_reader) + ["taxonomy"]
    csv_writer.writerow(header)
    # just do a try to catch when the cols headings are not as expected
    try:
        taxa_col = header.index("taxa_id")
    except:
        print("There needs to be a col called taxa_id")
        sys.exit(1)
    for row in csv_reader:
        taxa_id = row[taxa_col]
        taxonomy = taxa_dict[taxa_id]
        row.append(taxonomy)
        csv_writer.writerow(row)

# clean up
outfile_h.close()
