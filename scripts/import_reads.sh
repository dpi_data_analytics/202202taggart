#!/bin/bash

# import the demultiplexed data using a manifest file
# https://docs.qiime2.org/2021.11/tutorials/importing/#sequence-data-with-sequence-quality-information-i-e-fastq

qiime tools import \
  --type 'SampleData[PairedEndSequencesWithQuality]' \
  --input-path manifest.tsv.txt \
  --output-path paired-end-demux.qza \
  --input-format PairedEndFastqManifestPhred33V2

# generate a summary
qiime demux summarize \
  --i-data paired-end-demux.qza \
  --o-visualization paired-end-demux.qzv
