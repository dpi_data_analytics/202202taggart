#!/bin/bash

# extract the amplicon region from the silvia seqs  
# get the full length sequences
## wget https://data.qiime2.org/2020.8/common/silva-138-99-seqs.qza
# get the taxonomy
## wget https://data.qiime2.org/2020.8/common/silva-138-99-tax.qza
# move these files to ./db/silva

# primers 341F-806R
qiime feature-classifier extract-reads \
  --i-sequences ./db/silva/silva-138-99-seqs.qza \
  --p-f-primer CCTAYGGGRBGCASCAG \
  --p-r-primer GGACTACNNGGGTATCTAAT \
  --o-reads ./db/silva/silva-138-99-extracts.qza

qiime feature-classifier fit-classifier-naive-bayes \
  --i-reference-reads ./db/silva/silva-138-99-extracts.qza \
  --i-reference-taxonomy ./db/silva/silva-138-99-tax.qza \
  --o-classifier ./db/silva/silva-138-99-classifier.qza
