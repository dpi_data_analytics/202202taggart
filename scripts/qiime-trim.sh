#!/bin/bash

# the ^ limits the search to the start of the read
# Note the adaptors have already been removed from these reads, so they begin
# with the 16S primer sequences used.

# note
#This example shows trimming linked adapters as the amplicon is "framed" 
#by both a 5' and 3' adapter. This is equivalent to using the -a and -A 
#options in cutadapt and shows the forward primer linked to the reverse 
#complement of the reverse primer and vice versa. 
#The --p-front-f and --p-front-r options may also be used.

# so essentially you have the 5'3' F followed by the RC of the second primer.  
qiime cutadapt trim-paired \
  --i-demultiplexed-sequences paired-end-demux.qza  \
  --p-cores 24 \
  --p-adapter-f ^CCTAYGGGRBGCASCAG...ATTAGATACCCNNGTAGTCC \
  --p-adapter-r ^GGACTACNNGGGTATCTAAT...CTGSTGCVYCCCRTAGG \
  --p-error-rate 0.1 \
  --p-overlap 3 \
  --verbose \
  --o-trimmed-sequences paired-end-demux-trimmed.qza >> trim.log 2>&1

# visualise the results  
qiime demux summarize \
  --i-data paired-end-demux-trimmed.qza \
  --p-n 100000 \
  --o-visualization paired-end-demux-trimmed.qzv
