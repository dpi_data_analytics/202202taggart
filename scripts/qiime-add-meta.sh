#!/bin/bash

# add meta data to the table
qiime feature-table summarize \
  --i-table table-dada2.qza \
  --o-visualization table-dada2.qzv \
  --m-sample-metadata-file metadata.tsv.txt
