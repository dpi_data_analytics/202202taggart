# reformat the taxonomy for plotting
import csv

def make_label(taxonomy):
    levels = taxonomy.split()
    phyla = 'NA'
    family = 'NA'
    for level in levels:
        if level[:3] == 'p__':
            phyla = level[3:]
        if level[:3] == 'f__':
            family = level[3:]
    tag = "%s (Phylum: %s)" % (family, phyla)
    return tag

outfile = open('analyses/logfc_taxonomy_labels.csv', 'w')
csv_writer = csv.writer(outfile)
with open('analyses/logfc_taxonomy.csv') as rf:
    csv_reader = csv.reader(rf)
    header = next(csv_reader)
    csv_writer.writerow(header)
    for row in csv_reader:
        tag = make_label(row[-1])
        row[0] = tag
        csv_writer.writerow(row)


outfile.close()
