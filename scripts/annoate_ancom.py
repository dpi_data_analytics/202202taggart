# create phylum labels for plotting
import sys
import pandas as pd

def make_label(taxonomy):
    """crate taxonomy tags for plotting"""
    levels = taxonomy.split('; ')
    phyla = 'NA'
    family = 'NA'
    order = 'NA'
    for level in levels:
        if level[:3] == 'p__':
            phyla = level[3:]
        # collect the order for the uncultured
        if level[:3] == 'o__':
            order = level[3:]
            order = 'Order: %s' % order
        if level[:3] == 'f__':
            family = level[3:]
            #family = '(Family: %s)' % family
            if family == 'uncultured':
                family = order
            family = family.replace('[','')
            family = family.replace(']','')
    #return family
    tag = "%s (Phylum: %s)" % (family, phyla)
    return tag

# create a label column
def meta_label(row):
    label = '%s; %s (%s)'%(row['treatment'], row['sex'],row['sample-id'])
    return label

def make_small_label(taxonomy):
    return taxonomy.split(' (')[0]

infile = sys.argv[1]
df = pd.read_csv(infile)
df.head()

df['label'] = df['taxonomy'].apply(make_label)

print(set(df['label']))

meta_f = sys.argv[2]
#meta = pd.read_table('../qiime2/metadata.tsv.txt')
meta = pd.read_table(meta_f)
meta.tail()

meta['label'] = meta.apply(meta_label, axis=1)
meta

# make the phylum row first as this will be rownames in R
df_sub = pd.concat([df['label'], df.iloc[:,:47]], axis=1)
df_sub.head()

# fix the column headings to match figure 3
# add in the other column names that were not in the metadata
#for col in df.columns:
#    if col not in col_name_dict:
#        col_name_dict[col] = col
col_name_dict = dict(zip(meta['sample-id'], meta['label']))

col_name_dict['label'] = 'Phylum'
# now rename the cols
df_sub.rename(col_name_dict, axis=1, inplace=True)
#df_sub.head()
df_sub.sort_index(axis=1, inplace=True)

df_sub.to_csv('final_signif_log_obs_abn_adj_with_labels.csv', index=False)

df_sub2 = df_sub.copy(deep=True)
df_sub2['Phylum'] = df_sub2['Phylum'].apply(make_small_label)
df_sub2.head()

df_sub2.to_csv('final_signif_log_obs_abn_adj_with_labels_small.csv', index=False)
